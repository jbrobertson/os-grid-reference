# Size of the single and double letter top-level tiles
TILE_1_SIZE = 500_000
TILE_2_SIZE = 100_000

# Origin of the grid
GRID_ORIGIN_SW_Easting = - 2 * TILE_1_SIZE
GRID_ORIGIN_SW_Northing = - TILE_1_SIZE

# Order of the lettered tiles
LETTER_ORDER = "VWXYZ" \
               "QRSTU" \
               "LMNOP" \
               "FGHJK" \
               "ABCDE"

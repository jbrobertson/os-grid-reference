import pytest
import shapely.geometry as geo
import shapely.wkt as wkt
from os_grid_reference import ngr_decode


@pytest.mark.parametrize(
    'tile_name, expected_prefix, expected_n1, expected_n2, expected_suffix',
    [
        ("A", "A", None, None, None),
        ("AB", "AB", None, None, None),
        ("ASW", "A", None, None, "SW"),
        ("ABSW", "AB", None, None, "SW"),
        ("AB32SW", "AB", "3", "2", "SW"),
        ("AB32", "AB", "3", "2", None),
        ("AB123456", "AB", "123", "456", None),
        ("AB123456NE", "AB", "123", "456", "NE"),
    ])
def test_split_tile_id(tile_name, expected_prefix,
                       expected_n1, expected_n2, expected_suffix):
    prefix, n1, n2, suffix = ngr_decode._split_tile_id(tile_name)
    assert expected_prefix == prefix
    assert expected_n1 == n1
    assert expected_n2 == n2
    assert expected_suffix == suffix


@pytest.mark.parametrize(
    'tile_name, expected_easting, expected_northing, expected_size',
    [
        ("S", 0, 0, 500_000),
        ("TA034434", 503_400, 443_400, 100),
        ("HU396753", 439_600, 1_175_300, 100),
        ("NN12707012", 212700, 770120, 10),
        ("HU396753SW", 439600, 1175300, 50),
        ("HU396753NE", 439650, 1175350, 50),
        ("HU396753NW", 439600, 1175350, 50),
        ("HU396753SE", 439650, 1175300, 50),
    ])
def test_decode(tile_name, expected_easting,
                expected_northing, expected_size):
    tile = ngr_decode.ngr_decode(tile_name)
    assert tile.sw_easting == expected_easting
    assert tile.sw_northing == expected_northing
    assert tile.size == expected_size


@pytest.mark.parametrize(
    'tile_name, expected_geometry',
    [
        ("HU396753SE", geo.box(439650, 1175300, 439700, 1175350)),
    ])
def test_as_wkt(tile_name, expected_geometry):
    tile = ngr_decode.ngr_decode(tile_name)
    srid, geom = tile.as_wkt().split(";")
    assert "srid=27700" == srid
    assert expected_geometry.equals(wkt.loads(geom))


def test_as_dict():
    tile = ngr_decode.ngr_decode("HU396753SE")
    d = tile.as_dict()
    print(d)
    expected = {
        'sw_easting': 439650,
        'sw_northing': 1175300,
        'ne_easting': 439700,
        'ne_northing': 1175350,
        'size': 50.0
    }
    assert expected == d


#
# This is taken from the first table in
# https://digimap.edina.ac.uk/webhelp/os/data_information/os_data_issues/ng_converter.htm
#
@pytest.mark.parametrize(
    'tile_name, expected_size',
    [
        ("TA", 	100_000),
        ("TA04", 	10_000),
        ("TA04SW", 	5000),
        ("TA0343", 	1000),
        ("TA0343SW", 	500),
        ("TA034434", 	100),
        ("TA03434340", 	10),
        ("TA0343543405", 1),
    ])
def test_sizes(tile_name, expected_size):
    tile = ngr_decode.ngr_decode(tile_name)
    assert expected_size == tile.size

import pytest
from os_grid_reference import ngr_encode


@pytest.mark.parametrize("easting, northing, expected_letter", [
    (0.01, 0.01, 'S'),
    (-0.01, 0.01, 'R'),
    (-0.01, -0.01, 'W'),
    (0.01, -0.01, 'X'),
])
def test_tile_500km(easting, northing, expected_letter):
    letter = ngr_encode.tile_500km(easting, northing)
    assert letter == expected_letter


@pytest.mark.parametrize("easting, northing, expected_letters", [
    (0.01, 0.01, 'SV'),
    (-0.01, 0.01, 'RZ'),
    (-0.01, -0.01, 'WE'),
    (0.01, -0.01, 'XA'),
])
def test_tile_100km(easting, northing, expected_letters):
    letters = ngr_encode.tile_100km(easting, northing)
    assert letters == expected_letters


@pytest.mark.parametrize("easting, northing, expected_letters", [
    (0.01, 0.01, 'SV00'),
    (-0.01, 0.01, 'RZ90'),
    (-0.01, -0.01, 'WE99'),
    (0.01, -0.01, 'XA09'),
])
def test_tile_10km(easting, northing, expected_letters):
    letters = ngr_encode.tile_10km(easting, northing)
    assert letters == expected_letters


@pytest.mark.parametrize("easting, northing, expected_letters", [
    (0.01, 0.01, 'SV0000'),
    (-0.01, 0.01, 'RZ9900'),
    (-0.01, -0.01, 'WE9999'),
    (0.01, -0.01, 'XA0099'),
])
def test_tile_1km(easting, northing, expected_letters):
    letters = ngr_encode.tile_1km(easting, northing)
    assert letters == expected_letters


@pytest.mark.parametrize("easting, northing, expected_letters", [
    (0.01, 0.01, 'SV0000'),
    (-0.01, 0.01, 'RZ9900'),
    (-0.01, -0.01, 'WE9999'),
    (0.01, -0.01, 'XA0099'),
])
def test_tile_1km_alt(easting, northing, expected_letters):
    letters = ngr_encode.tile_n(easting, northing, 4)
    assert letters == expected_letters


@pytest.mark.parametrize("easting, northing, expected_letters", [
    (503400.01, 443400.01, 'TA034434'),

])
def test_tile_100m_alt(easting, northing, expected_letters):
    letters = ngr_encode.tile_n(easting, northing, 6)
    assert letters == expected_letters
